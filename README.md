Twitch Past Broadcast Bot
=========================

Setup
-----

This script requires python 3 praw, and praw-oauth2util.

To install praw and praw-oauth2util type in your command line:

    pip install praw
    pip install praw-oauth2util


OAuth Setup
-----------

You will need to created a reddit app to be able to run this (https://www.reddit.com/prefs/apps/).
When you created it, select "Personal Use Script" from the options, and set the `redirect_uri` to `http://127.0.0.1:65010/authorize_callback`. All the other options are up to you to fill in. After that you will need to get the
app key and app secret from the same page and add them to the corresponding variables in the oauth.txt file.


Config
------

Add the subreddit name you want the bot to post to to the SUBREDDIT variable (don't include the /r/).
Then add the name of the channel you want it to fetch the past broadcast from to the CHANNEL_NAME variable.

The bot will attempt to pull the video from twitch and post it to your subreddit of choice, and then sleep for an hour and post a new video if one has appeared since it last tried.
