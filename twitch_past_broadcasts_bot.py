import time
import praw
import requests
import OAuth2Util

SUBREDDIT = ''
CHANNEL_NAME = ''
POST_TITLE = "[{}] {} {}"
REQUEST_URL = 'https://api.twitch.tv/kraken/channels/{}/videos?limit=1&broadcasts=true'


def get_past_broadcast(r):
    print("Requesting video...")
    request = requests.get(REQUEST_URL.format(CHANNEL_NAME))
    json = request.json()

    title = json['videos'][0]['title']
    game = json['videos'][0]['game']
    url = json['videos'][0]['url']
    date = json['videos'][0]['recorded_at'][:10]

    post_video(r, title, game, url, date)


def post_video(r, title, game, url, date):
    date = "{}/{}/{}".format(date[5:7], date[8:], date[:4])
    t = POST_TITLE.format(game, title, date)
    try:
        print("Posting video...")
        r.submit(SUBREDDIT, t, url=url)
    except praw.errors.AlreadySubmitted:
        print("Already submitted skipping...")


def main():
    r = praw.Reddit('Posts_Twitch_Past_Broadcasts v1.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        try:
            o.refresh()
            get_past_broadcast(r)
            print("Sleeping...")
            time.sleep(3600)  # sleep for an hour
        except Exception as e:
            print('ERROR: {}'.format(e))
            time.sleep(300)

if __name__ == "__main__":
    main()
